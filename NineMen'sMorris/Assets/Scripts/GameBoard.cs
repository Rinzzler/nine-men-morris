﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoard : MonoBehaviour {

    int[] posArray = new int[] {0, 1, 2, 7, 3, 6, 5, 4};
    public static string[,] boardPosValue = new string[3,8];
    public static GameObject selectedFigure;
    public static List<GameObject> whiteFigures = new List<GameObject>();
    public static List<GameObject> blackFigures = new List<GameObject>();
    public static List<GameObject> emptySlots = new List<GameObject>();
    public static string playerTurn = "white";
    public static int gamePhase = 1;
    public GameObject emptySlot;

    // Use this for initialization
    private void Start()
    {
        ResetData();
    }

    public void ResetData()
    {
        foreach (GameObject slot in emptySlots)
            Destroy(slot);
        emptySlots.Clear();
        for (int i = 0; i < 3; i++)
        {
            int k = 0;
            for (int x = -1; x < 2; x++)
                for (int y = -1; y < 2; y++)
                    if (x != 0 || y != 0)
                    {
                        GameObject instance = Instantiate(emptySlot, new Vector3(x * (i+1), y * (i+1), 0f), Quaternion.identity) as GameObject;
                        boardPosValue[i, posArray[k]] = "empty";
                        instance.GetComponent<EmptySlotScript>().boardPosIndex1 = i;
                        instance.GetComponent<EmptySlotScript>().boardPosIndex2 = posArray[k];
                        emptySlots.Add(instance);
                        k++;
                    }
        }
        gamePhase = 1;
        foreach (GameObject figure in whiteFigures)
            Destroy(figure);
        whiteFigures.Clear();
        foreach (GameObject figure in blackFigures)
            Destroy(figure);
        blackFigures.Clear();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
