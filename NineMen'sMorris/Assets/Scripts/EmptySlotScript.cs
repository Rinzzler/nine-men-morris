﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptySlotScript : MonoBehaviour {

    public GameObject gameManager;
    GameManager gameManagerScript;
    bool mouseOverObject = false;
    public int squarePos;
    public Dictionary<int, int> boardPosIndex;
    public int boardPosIndex1;
    public int boardPosIndex2;

    // Use this for initialization
    void Start () {

    }


    private void OnMouseEnter()
    {
        mouseOverObject = true;
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        StartCoroutine(animateCircle());
    }

    private void OnMouseExit()
    {
        mouseOverObject = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        StopCoroutine(animateCircle());
    }

    private IEnumerator animateCircle()
    {
        float speed = 20f;
        while (mouseOverObject)
        {
            gameObject.transform.Rotate(Vector3.forward, speed * Time.deltaTime);
            yield return null;
        }
    }

    private void OnMouseDown()
    {
        if (GameManager.action == "selectMove")
            gameManager.GetComponent<GameManager>().MoveToEmptySlot(gameObject);
        else gameManager.GetComponent<GameManager>().EmptySlotSelected(gameObject);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
