﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameCanvasScript : MonoBehaviour {

    public GameObject panel;
    public TextMeshProUGUI winnerText;
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void EndGame(string winner)
    {
        panel.SetActive(true);
        winnerText.SetText("The winner is " + winner);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MenuScene");
        GameManager.instance.InterruptGame();
    }
}
