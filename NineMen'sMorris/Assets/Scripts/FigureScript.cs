﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureScript : MonoBehaviour {

    public GameManager gameManager;
    public GameObject figure;
    public GameObject circle;
    public string figureColor;
    bool mouseOverObject = false;
    //public Dictionary<int, int> boardPosIndex;
    public int boardPosIndex1;
    public int boardPosIndex2;
    
    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnMouseEnter()
    {
        mouseOverObject = true;
        ActivateCircle();
    }

    private void OnMouseExit()
    {
        mouseOverObject = false;
        circle.GetComponent<SpriteRenderer>().enabled = false;
        StopCoroutine(animateCircle());
    }

    public void ActivateCircle()
    {
        mouseOverObject = true;
        circle.GetComponent<SpriteRenderer>().enabled = true;
        StartCoroutine(animateCircle());
    }

    private IEnumerator animateCircle()
    {
        float speed = 20f;
        while (mouseOverObject)
        {
            circle.transform.Rotate(Vector3.forward, speed * Time.deltaTime);
            yield return null;
        }
    }

    public void MoveToSlot(Vector3 target, int index1, int index2)
    {
        boardPosIndex1 = index1;
        boardPosIndex2 = index2;
        StartCoroutine(MoveToTarget(target));
        circle.GetComponent<SpriteRenderer>().enabled = false;
    }

    private IEnumerator MoveToTarget(Vector3 target)
    {
        float fraction = 0;
        float speed = 2f;
        Vector3 start = transform.position;
        while (fraction < 1)
        {
            fraction += (Time.deltaTime * speed);
            gameObject.transform.position = Vector3.Lerp(start, target, fraction);
            yield return null;
        }
    }

    private void OnMouseDown()
    {
        bool clicked = false;
        if (GameManager.action == "remove")
        {
            gameManager.DestroyFigure(gameObject, figureColor);
            clicked = true;
        }
        if (!clicked)
            if (GameManager.action == "selectMove")
            {
                circle.GetComponent<SpriteRenderer>().enabled = true;
                gameManager.ChooseWhereToMove(gameObject);
            }
    }

}
