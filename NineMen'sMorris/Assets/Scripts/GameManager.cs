﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public GameObject blackFigure;
    public GameObject whiteFigure;
    public GameObject emptySlot;
    public static string action;
    GameObject selectedFigure;
    public static Stack whiteFigureStack = new Stack();
    public static Stack blackFigureStack = new Stack();
    public TextMeshProUGUI playerTurnText, figureCountText, instructionText, flyingText;
    public GameObject canvas;


    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        SpawnPlayerFigures();
        action = "";
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnPlayerFigures()
    {
        for (int i=0; i < 9; i++)
        {
            GameObject white = Instantiate(whiteFigure, new Vector3(-4.5f, (-4 + i), 0f), Quaternion.identity) as GameObject;
            white.layer = 2;
            whiteFigureStack.Push(white);
            GameObject black = Instantiate(blackFigure, new Vector3(4.5f, (-4 + i), 0f), Quaternion.identity) as GameObject;
            black.layer = 2;
            blackFigureStack.Push(black);
        }
        PlaceNextFigure();
    }

    public void PlaceNextFigure()
    {
        instance.playerTurnText.SetText("Player: " + GameBoard.playerTurn);
        instance.instructionText.SetText("Place figure on board");
        GameBoard.selectedFigure = SelectNextFigure();
        var figureScript = GameBoard.selectedFigure.GetComponent<FigureScript>();
        figureScript.ActivateCircle();
    }

    public GameObject SelectNextFigure()
    {
        if (GameBoard.playerTurn == "white")
            return whiteFigureStack.Pop() as GameObject;
        else return blackFigureStack.Pop() as GameObject;
    }

    public void EmptySlotSelected(GameObject slot)
    {
        EmptySlotScript slotScript = slot.GetComponent<EmptySlotScript>();

        GameBoard.boardPosValue[slotScript.boardPosIndex1, slotScript.boardPosIndex2] = GameBoard.selectedFigure.GetComponent<FigureScript>().figureColor;
        GameBoard.selectedFigure.GetComponent<FigureScript>().MoveToSlot(slot.transform.position, slotScript.boardPosIndex1, slotScript.boardPosIndex2);
        if (GameBoard.selectedFigure.GetComponent<FigureScript>().figureColor == "white")
        {
            GameBoard.whiteFigures.Add(GameBoard.selectedFigure);
        }
        else
        {
            GameBoard.blackFigures.Add(GameBoard.selectedFigure);
        }
        GameBoard.emptySlots.Remove(slot);
        Destroy(slot);
        CheckifMill();
    }

    public void CheckifMill()
    {
        int boardPosIndex1 = GameBoard.selectedFigure.GetComponent<FigureScript>().boardPosIndex1;
        int boardPosIndex2 = GameBoard.selectedFigure.GetComponent<FigureScript>().boardPosIndex2;
        bool millFound = false;
        if (boardPosIndex2 % 2 == 1)
        {
            if (GameBoard.boardPosValue[0, boardPosIndex2] == GameBoard.boardPosValue[1, boardPosIndex2])
            {
                if (GameBoard.boardPosValue[1, boardPosIndex2] == GameBoard.boardPosValue[2, boardPosIndex2])
                {
                    millFound = true;
                }
            }
            if (GameBoard.boardPosValue[boardPosIndex1, boardPosIndex2 - 1] == GameBoard.boardPosValue[boardPosIndex1, boardPosIndex2])
                if (GameBoard.boardPosValue[boardPosIndex1, boardPosIndex2] == GameBoard.boardPosValue[boardPosIndex1, (boardPosIndex2 + 1) % 8])
                {
                    millFound = true;
                }
        }
        else
        {
            if (GameBoard.boardPosValue[boardPosIndex1, boardPosIndex2] == GameBoard.boardPosValue[boardPosIndex1, (boardPosIndex2 + 1) % 8])
            {
                if (GameBoard.boardPosValue[boardPosIndex1, (boardPosIndex2 + 1) % 8] == GameBoard.boardPosValue[boardPosIndex1, (boardPosIndex2 + 2) % 8])
                {
                    millFound = true;
                }
            }
            if (GameBoard.boardPosValue[boardPosIndex1, boardPosIndex2] == GameBoard.boardPosValue[boardPosIndex1, (8 + (boardPosIndex2 - 1)) % 8])
                if (GameBoard.boardPosValue[boardPosIndex1, (8 + (boardPosIndex2 - 1)) % 8] == GameBoard.boardPosValue[boardPosIndex1, (8 + (boardPosIndex2 - 2)) % 8])
                {
                    millFound = true;
                }
        }

        if (millFound == true)
            RemoveOneFigure();
        else if (GameBoard.gamePhase == 1)
            EndTurn();
        else EndTurn2();
    }

    void RemoveOneFigure()
    {
        foreach (GameObject emptySlot in GameBoard.emptySlots)
            emptySlot.layer = 2;
        List<GameObject> listToRemoveFrom;
        if (GameBoard.playerTurn == "white")
        {
            instance.instructionText.SetText("You formed a mill. Remove one black figure");
            listToRemoveFrom = GameBoard.blackFigures;
        }
        else
        {
            instance.instructionText.SetText("You formed a mill. Remove one white figure");
            listToRemoveFrom = GameBoard.whiteFigures;
        }
        foreach (GameObject figure in listToRemoveFrom)
        figure.layer = 0;
        action = "remove";
    }

    public void DestroyFigure(GameObject toDestroy, string figureColor)
    {
        List<GameObject> listToRemoveFrom;
        if (figureColor == "white")
            listToRemoveFrom = GameBoard.whiteFigures; 
        else listToRemoveFrom = GameBoard.blackFigures;
        listToRemoveFrom.Remove(toDestroy);
        GameObject instance = Instantiate(emptySlot, toDestroy.transform.position, Quaternion.identity) as GameObject;
        instance.GetComponent<EmptySlotScript>().boardPosIndex1 = toDestroy.GetComponent<FigureScript>().boardPosIndex1;
        instance.GetComponent<EmptySlotScript>().boardPosIndex2 = toDestroy.GetComponent<FigureScript>().boardPosIndex2;
        GameBoard.emptySlots.Add(instance);
        GameBoard.boardPosValue[toDestroy.GetComponent<FigureScript>().boardPosIndex1, toDestroy.GetComponent<FigureScript>().boardPosIndex2] = "empty";
        Destroy(toDestroy);
        foreach (GameObject figure in listToRemoveFrom)
            figure.layer = 2;
        if (GameBoard.gamePhase == 1)
            EndTurn();
        else EndTurn2();
    }


    public void EndTurn()
    { 
        foreach (GameObject emptySlot in GameBoard.emptySlots)
            emptySlot.layer = 0;
        if (GameBoard.playerTurn == "white")
            GameBoard.playerTurn = "black";
        else GameBoard.playerTurn = "white";
        if (whiteFigureStack.Count == 0 && blackFigureStack.Count == 0)
            PhaseTwo();
        else PlaceNextFigure();
    }

    public void PhaseTwo()
    {
        GameBoard.gamePhase = 2;
        instance.instructionText.SetText("Choose a figure to move");
        ChooseFigureToMove();
    }

    void ChooseFigureToMove()
    {
        instance.playerTurnText.SetText("Player: " + GameBoard.playerTurn);
        List<GameObject> availableFigures;
        foreach (GameObject emptySlot in GameBoard.emptySlots)
            emptySlot.layer = 2;
        if (GameBoard.playerTurn == "white")
            availableFigures = GameBoard.whiteFigures;
        else availableFigures = GameBoard.blackFigures;
        instance.figureCountText.SetText("Figures left: " + availableFigures.Count);
        if (availableFigures.Count < 4)
            instance.flyingText.SetText("You are in Flying phase!\nYou can move your figures anywhere on the board");
        else instance.flyingText.SetText("");
        foreach (GameObject figure in availableFigures)
            figure.layer = 0;
        action = "selectMove";
    }

    public void ChooseWhereToMove(GameObject figure)
    {
        instance.instructionText.SetText("Choose where to move the figure");
        GameBoard.selectedFigure = figure;
        List<GameObject> activeFigures;
        if (GameBoard.playerTurn == "white")
            activeFigures = GameBoard.whiteFigures;
        else activeFigures = GameBoard.blackFigures;
        foreach (GameObject figures in activeFigures)
            figures.layer = 2;
        foreach (GameObject emptySlot in GameBoard.emptySlots)
            emptySlot.layer = 2;
        GameBoard.selectedFigure.GetComponent<FigureScript>().ActivateCircle();
        CheckAvailableSlots();
    }

    void CheckAvailableSlots()
    {
        bool canMove = false;
        int numOfFigures;
        if (GameBoard.playerTurn == "white")
            numOfFigures = GameBoard.whiteFigures.Count;
        else numOfFigures = GameBoard.blackFigures.Count;
        if (numOfFigures > 3)
        {
            int index1 = GameBoard.selectedFigure.GetComponent<FigureScript>().boardPosIndex1;
            int index2 = GameBoard.selectedFigure.GetComponent<FigureScript>().boardPosIndex2;
            if (index2 % 2 == 1)
                foreach (GameObject emptySlot in GameBoard.emptySlots)
                {
                    if ((emptySlot.GetComponent<EmptySlotScript>().boardPosIndex1 == index1) && (emptySlot.GetComponent<EmptySlotScript>().boardPosIndex2 == index2 - 1))
                    {
                        canMove = true;
                        emptySlot.layer = 0;
                    }
                    if ((emptySlot.GetComponent<EmptySlotScript>().boardPosIndex1 == index1) && (emptySlot.GetComponent<EmptySlotScript>().boardPosIndex2 == (index2 + 1) % 8))
                    {
                        canMove = true;
                        emptySlot.layer = 0;
                    }
                    if ((emptySlot.GetComponent<EmptySlotScript>().boardPosIndex1 == index1 + 1) && (emptySlot.GetComponent<EmptySlotScript>().boardPosIndex2 == index2))
                    {
                        canMove = true;
                        emptySlot.layer = 0;
                    }
                    if ((emptySlot.GetComponent<EmptySlotScript>().boardPosIndex1 == index1 - 1) && (emptySlot.GetComponent<EmptySlotScript>().boardPosIndex2 == index2))
                    {
                        canMove = true;
                        emptySlot.layer = 0;
                    }
                }
            else foreach (GameObject emptySlot in GameBoard.emptySlots)
                {
                    if ((emptySlot.GetComponent<EmptySlotScript>().boardPosIndex1 == index1) && (emptySlot.GetComponent<EmptySlotScript>().boardPosIndex2 == (8 + (index2 - 1)) % 8))
                    {
                        canMove = true;
                        emptySlot.layer = 0;
                    }
                    if ((emptySlot.GetComponent<EmptySlotScript>().boardPosIndex1 == index1) && (emptySlot.GetComponent<EmptySlotScript>().boardPosIndex2 == index2 + 1))
                    {
                        canMove = true;
                        emptySlot.layer = 0;
                    }
                }
        }
        else foreach (GameObject emptySlot in GameBoard.emptySlots)
            {
                canMove = true;
                emptySlot.layer = 0;
            }
        if (!canMove)
        {
            instance.instructionText.SetText("That figure has nowhere to be moved!\nChoose another figure");
            ChooseFigureToMove();
        }

    }
    
    public void MoveToEmptySlot(GameObject newSlot)
    {
        Vector3 newEmptySlotPosition = GameBoard.selectedFigure.transform.position;
        int index1 = GameBoard.selectedFigure.GetComponent<FigureScript>().boardPosIndex1;
        int index2 = GameBoard.selectedFigure.GetComponent<FigureScript>().boardPosIndex2;
        GameBoard.boardPosValue[GameBoard.selectedFigure.GetComponent<FigureScript>().boardPosIndex1, GameBoard.selectedFigure.GetComponent<FigureScript>().boardPosIndex2] = "empty";

        EmptySlotScript slotScript = newSlot.GetComponent<EmptySlotScript>();
        
        GameBoard.boardPosValue[slotScript.boardPosIndex1, slotScript.boardPosIndex2] = GameBoard.selectedFigure.GetComponent<FigureScript>().figureColor;
        GameBoard.selectedFigure.GetComponent<FigureScript>().MoveToSlot(newSlot.transform.position, slotScript.boardPosIndex1, slotScript.boardPosIndex2);
        newSlot.transform.position = newEmptySlotPosition;
        newSlot.GetComponent<EmptySlotScript>().boardPosIndex1 = index1;
        newSlot.GetComponent<EmptySlotScript>().boardPosIndex2 = index2;
        CheckifMill();
    }

    public void EndTurn2()
    {
        if (GameBoard.whiteFigures.Count < 3 || GameBoard.blackFigures.Count < 3)
            GameOver();
        else
        {
            action = "selectMove";
            if (GameBoard.playerTurn == "white")
                GameBoard.playerTurn = "black";
            else GameBoard.playerTurn = "white";
            instance.instructionText.SetText("Choose a figure to move");
            ChooseFigureToMove();
        }
    }

    void GameOver()
    {
        foreach (GameObject figure in GameBoard.whiteFigures)
            figure.layer = 2;
        foreach (GameObject figure in GameBoard.blackFigures)
            figure.layer = 2;
        foreach (GameObject slot in GameBoard.emptySlots)
            slot.layer = 2;
        while (whiteFigureStack.Count > 0)
        {
            GameObject toBeDestroyed = whiteFigureStack.Pop() as GameObject;
            Destroy(toBeDestroyed);
        }
        while (whiteFigureStack.Count > 0)
        {
            GameObject toBeDestroyed = blackFigureStack.Pop() as GameObject;
            Destroy(toBeDestroyed);
        }
        instance.canvas.GetComponent<GameCanvasScript>().EndGame(GameBoard.playerTurn);
    }

    public void InterruptGame()
    {
        while (whiteFigureStack.Count > 0)
        {
            GameObject toBeDestroyed = whiteFigureStack.Pop() as GameObject;
            Destroy(toBeDestroyed);
        }
        while (blackFigureStack.Count > 0)
        {
            GameObject toBeDestroyed = blackFigureStack.Pop() as GameObject;
            Destroy(toBeDestroyed);
        }
    }
}
