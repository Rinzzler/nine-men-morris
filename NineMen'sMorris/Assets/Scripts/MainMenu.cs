﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void WhiteFirst()
    {
        GameBoard.playerTurn = "white";
        SceneManager.LoadScene("GameScene");
    }

    public void BlackFirst()
    {
        GameBoard.playerTurn = "black";
        SceneManager.LoadScene("GameScene");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
